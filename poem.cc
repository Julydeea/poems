#include <algorithm>
#include <iostream>
#include <set>
#include <sstream>
#include <vector>
#include <unordered_map>

using namespace std;

class Poetry
{
	/*storage for the already found rhymes
	and their identification token*/
	unordered_map<string, char> rhymes;
	/*solution string*/
	string letters;

public:
	/*identifies the rhyme part of the word*/
	string getRhyme(string word)
	{
		set<int> vowels = { 'a', 'e', 'i', 'o', 'u' };
		set<int> y_vowels = { 'a', 'e', 'i', 'o', 'u', 'y' };

		if (word[0] < 'a')
		{
			string l_word(word);
			transform(word.begin(), word.end(), word.begin(), ::tolower);
		}
		bool foundNonVowel = false;
		int index = -1;
		for (int i = 0; i < word.size(); i++)
		{
			bool foundVowel = false;
			if (i == 0 || i == word.size() - 1)
			{
				if (vowels.find(word[i]) != vowels.end())
					foundVowel = true;
			}
			else
			{
				if (y_vowels.find((int)word[i]) != y_vowels.end())
					foundVowel = true;
			}
			if (foundVowel)
			{
				if (foundNonVowel && index != -1)
				{
					foundNonVowel = false;
					index = i;
				}
				else
				{
					if (index == -1)
					{
						foundNonVowel = false;
						index = i;
					}
				}
			}
			else
				foundNonVowel = true;

		}

		return word.substr(index, word.length() - index);

	}

	/*builds the rhyme scheme*/
	string rhymeScheme(const vector <string>& poem)
	{
		char last_char = ' ';
		/*loop through all the words in the poem*/
		for (int i = 0; i < poem.size(); i++)
		{
			string line = poem[i];
			if (!all_of(line.begin(), line.end(), isspace) && !line.empty())
			{
				size_t j = line.length() - 1;
				while (line[j] == ' ')
					j--;

				size_t index = line.rfind(' ', j);
				string word;
				if (index == string::npos)
					word = line.substr(0, j + 1);
				else
					word = line.substr(index + 1, j - index);

				/*separating the rhyme from the word*/
				word = getRhyme(word);
				/*populating the rhyme schema and adding found rhymes
				to the hash map for future indetification*/
				if (rhymes.empty())
				{
					rhymes[word] = 'a';
					last_char = 'a';
					letters.push_back(last_char);
				}
				else
				{
					if (rhymes.find(word) != rhymes.end())
						letters.push_back(rhymes[word]);
					else
					{
						if (last_char == 'z')
							last_char = 'A';
						else
							last_char++;
						rhymes[word] = last_char;
						letters.push_back(last_char);
					}
				}
			}
			else
			{
				letters.push_back(' ');
			}
		}

		return letters;
	}
};

int main()
{
	vector<string> poem = { "This poem has uppercase letters",
		"In its rhyme scheme",
		"Alpha", "Blaster", "Cat", "Desert", "Elephant", "Frog", "Gulch",
		"Horse", "Ireland", "Jam", "Krispy Kreme", "Loofah", "Moo", "Narf",
		"Old", "Pink", "Quash", "Rainbow", "Star", "Tour", "Uvula", "Very",
		"Will", "Xmas", "Young", "Zed", "deception", "comic", "grout",
		"oval", "cable", "rob", "steal", "steel", "weak" };
	Poetry po;
	cout << po.rhymeScheme(poem);

	return 0;
}